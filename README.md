# Agile Engine React Native Demo App #

1. Install [Expo] application on Android or iOS device
2. Open "https://exp.host/@olegderecha/agile-engine-rn-demo" link.
3. Login/password - any chars but must be the same.

## Local Development Environment Setup ##

1. Install [Node.js](https://nodejs.org/en/download/current/)
2. Install [Expo](https://docs.expo.io/versions/v17.0.0/introduction/installation.html)
3. Clone the React Native App repository
4. Run ```npm i``` at the root of your local project folder
5. Launch [Expo XDE](https://docs.expo.io/versions/v17.0.0/introduction/xde-tour.html) (sign up if needed) and open the Agile Engine Demo project
6. Setup either Android or iOS simulator or attach a real device
7. Press the Device button on the XDE toolbar to run the app
8. Use Visual Studio Code (recommended) or any other JS editor for coding
