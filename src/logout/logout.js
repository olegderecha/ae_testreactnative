import React from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { translate } from "react-i18next"

import { View, Text } from "react-native"
import { Button } from "react-native-elements"

import { Header } from "./components"

import styles from "./styles"

export default (LogoutScreen = translate("logout")(({ t, onLogout }) =>
  <View style={styles.screen}>
    <Header title={t("title")} />
    <View style={styles.content}>
      <Text style={styles.contentText}>
        {t("text")}
      </Text>
      <Button
        buttonStyle={styles.logoutButton}
        fontWeight="bold"
        backgroundColor="13b0bc"
        title={t("button")}
        onPress={onLogout}
      />
    </View>
  </View>
))

LogoutScreen.propTypes = {
  onLogout: PropTypes.func.isRequired
}
