import { connect } from "react-redux"

import LogoutScreen from "./logout"

import authActions from "../app/actions/auth"

const mapStateToProps = (state, props) => ({})
const mapDispatchToProps = (dispatch, props) => ({
  onLogout: () => dispatch(authActions.logOut())
})

export default connect(mapStateToProps, mapDispatchToProps)(LogoutScreen)
