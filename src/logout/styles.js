import { StyleSheet, Dimensions } from "react-native"

import colors from "../app/colors"

export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: colors.white
  },

  content: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },

  contentText: {
    fontSize: 30
  },

  logoutButton: {
    height: 48,
    width: 120,
    backgroundColor: colors.blueSky,
    borderRadius: 8,
    marginTop: 40,
    marginLeft: 0,
    marginRight: 0,
    alignSelf: "center"
  }
})
