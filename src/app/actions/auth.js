import { createActions } from "redux-actions"
import { NavigationActions } from "react-navigation"
import i18n from "../i18n"

const actions = createActions({
  logIn: {
    start: x => x,
    success: x => x,
    error: error => ({ error }),
    clearError: x => x
  },
  logOut: x => x
})

export default actions

export const logIn = ({ username, password }) => async dispatch => {
  dispatch(actions.logIn.start())

  const errorResult = { status: 0, error: i18n.t("login:loginError") }
  const successResult = {
    status: 1,
    user: {
      username: username
    }
  }

  const isValid =
    !!username.length && !!password.length && username === password
  const result = isValid ? successResult : errorResult

  try {
    // TODO: call API here
    // const result = await api....

    if (!result.status) {
      dispatch(actions.logIn.error(result.error))
      return
    }

    dispatch(actions.logIn.success({ user: result.user }))
    dispatch(navigateAfterLogin())
  } catch (e) {
    dispatch(actions.logIn.error(e.message))
  }
}

export const logOut = () => async dispatch => {
  dispatch(navigateToLoginScreen())
  dispatch(actions.logOut())
}

const navigateAfterLogin = () => dispatch => {
  dispatch(
    NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Home" })]
    })
  )
}

const navigateToLoginScreen = () => dispatch => {
  dispatch(
    NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Login" })]
    })
  )
}
