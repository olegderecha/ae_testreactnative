import datetimeService from "../services/datetime"

export default item => {
  return {
    title: item.title,
    activityDate: datetimeService.getFormattedDate(item.last_activity_date),
    answers: item.answer_count,
    isAnswered: item.is_answered,
    score: item.score,
    tags: item.tags.join(", ")
  }
}
