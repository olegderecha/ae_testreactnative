export default {
  blueSky: "#33bdef",
  blueDark: "#267CB5",
  black: "#000000",
  white: "#FFFFFF",
  grey: "#B7C4CD",
  darkGrey: "#6E767B",
  lightGrey: "#E0E0E0",
  ultraLightGrey: "#F4F6F9",
  lightGreen: "#E8FBEF",
  darkGreen: "#499065",

  background: "#EDEEEF",
  headerBackground: "#393c46",
  menuBackground: "#c4cad8",
  errorBackground: "#D8553F",
  buttonBorder: "#bddbfa",
  questionTitle: "#326EB6",
  questionTag: "#AEAEAE",
  questionVote: "#7E8385",

  spinner: "#00afbc",
  overlap: "rgba(129, 179, 198, 0.3)",
  linear: ["#cae5ec", "#f0f1ea"]
}
