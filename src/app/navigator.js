import React from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { addNavigationHelpers, StackNavigator } from "react-navigation"

import LoginScreen from "../login"
import HomeScreen from "../home"
import StackoverflowScreen from "../stackoverflow"
import LogoutScreen from "../logout"

export const AppNavigator = StackNavigator(
  {
    Login: { screen: LoginScreen },
    Home: { screen: HomeScreen },
    Stackoverflow: { screen: StackoverflowScreen },
    Logout: { screen: LogoutScreen }
  },
  {
    headerMode: "none",
    cardStyle: {
      backgroundColor: "black",
      opacity: 0.99
    }
  }
)

const AppWithNavigationState = ({ dispatch, nav }) =>
  <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />

AppWithNavigationState.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nav: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  nav: state.nav
})

export default connect(mapStateToProps)(AppWithNavigationState)
