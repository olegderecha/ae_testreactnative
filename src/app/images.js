export default {
  menu: require("../../assets/images/menu-icon.png"),

  question: {
    votes: require("../../assets/images/question-votes.png"),
    answersYes: require("../../assets/images/question-answers-yes.png"),
    answersNo: require("../../assets/images/question-answers-no.png"),
    answerAccepted: require("../../assets/images/question-answer-accepted.png")
  },

  login: {
    username: require("../../assets/images/login-username.png"),
    password: require("../../assets/images/login-password.png")
  },

  navigate: {
    home: require("../../assets/images/navigate-home.png"),
    stackoverflow: require("../../assets/images/navigate-stackoverflow.png"),
    logOut: require("../../assets/images/navigate-log-out.png")
  }
}
