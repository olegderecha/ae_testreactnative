import React from "react"
import { View, Keyboard } from "react-native"

export class KeyboardAvoiding extends React.Component {
  state = {
    keyboardHeight: 0
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this.keyboardDidShow
    )
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this.keyboardDidHide
    )
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  keyboardDidShow = e => {
    this.setState({ keyboardHeight: e.endCoordinates.height })
  }

  keyboardDidHide = () => {
    this.setState({ keyboardHeight: 0 })
  }

  render() {
    return (
      <View style={{ marginBottom: this.state.keyboardHeight }}>
        {this.props.children}
      </View>
    )
  }
}
