import { StyleSheet } from "react-native"

import colors from "../../colors"

export default StyleSheet.create({
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: colors.grey,
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 8,
    paddingTop: 32,
    height: 80
  },

  headerMenu: {
    width: 38,
    height: 30
  },

  title: {
    flex: 1,
    textAlign: "center",
    fontSize: 20
  }
})
