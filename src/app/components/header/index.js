import React from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"

import { NavigationActions } from "react-navigation"

import { View, Image, TouchableHighlight, Text } from "react-native"

import images from "../../images"
import colors from "../../colors"
import styles from "./styles"

export const HeaderComponent = ({ title, onMenuPress }) =>
  <View style={styles.header}>
    <TouchableHighlight underlayColor={colors.darkGrey} onPress={onMenuPress}>
      <Image style={styles.headerMenu} source={images.menu} />
    </TouchableHighlight>
    <Text style={styles.title}>
      {title}
    </Text>
  </View>

HeaderComponent.propTypes = {
  title: PropTypes.string.isRequired,
  onMenuPress: PropTypes.func.isRequired
}

const mapStateToProps = (state, props) => ({})
const mapDispatchToProps = (dispatch, props) => ({
  onMenuPress: route =>
    dispatch(NavigationActions.navigate({ routeName: "DrawerOpen" }))
})

export const Header = connect(mapStateToProps, mapDispatchToProps)(
  HeaderComponent
)
