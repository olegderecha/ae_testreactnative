import React from "react"
import PropTypes from "prop-types"

import { View, ActivityIndicator } from "react-native"

import styles from "./styles"
import colors from "../../colors"

export const ActivitySpinner = ({ isVisible, size }) =>
  !!isVisible &&
  <View style={styles.spinnerView}>
    <ActivityIndicator
      animating={isVisible}
      color={colors.spinner}
      size={size}
    />
  </View>

ActivitySpinner.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  size: PropTypes.string
}

ActivitySpinner.defaultProps = {
  size: "large"
}
