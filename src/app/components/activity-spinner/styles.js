import { StyleSheet } from "react-native"

import colors from "../../colors"

export default StyleSheet.create({
  spinnerView: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.overlap
  }
})
