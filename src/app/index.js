import React from "react"
import { View } from "react-native"

import { connect, Provider } from "react-redux"
import { I18nextProvider, translate } from "react-i18next"

import AppWithNavigationState from "./navigator"

import store from "./store"
import i18n from "./i18n"

export class App extends React.Component {
  render() {
    return (
      <I18nextProvider i18n={i18n}>
        <Provider store={store}>
          <View style={{ flex: 1 }}>
            <AppWithNavigationState />
          </View>
        </Provider>
      </I18nextProvider>
    )
  }
}
