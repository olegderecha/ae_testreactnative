import i18n from "i18next"
import Expo from "expo"

import resources from "./strings"

// creating a language detection plugin using expo
// http://i18next.com/docs/ownplugin/#languagedetector
const languageDetector = {
  type: "languageDetector",
  async: true,
  detect: cb =>
    Expo.Util.getCurrentLocaleAsync().then(lng => {
      const locale = lng.replace("_", "-")
      cb(locale)
    }),
  init: () => {},
  cacheUserLanguage: () => {}
}

i18n.use(languageDetector).init({
  fallbackLng: "en",

  resources,

  debug: !!__DEV__,

  defaultNS: "",
  interpolation: { escapeValue: false },

  react: { wait: true }
})

i18n.getLocale = () => {
  const language = i18n.language.toLowerCase()
  if (language.startsWith("fr")) {
    return "fr"
  } else {
    return "en"
  }
}

export default i18n
