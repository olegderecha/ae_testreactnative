import { combineReducers } from "redux"

import actions from "../actions/auth"

import authentication from "./authentication"
import questions from "./questions"
import nav from "./navigation"

const appReducer = combineReducers({
  authentication,
  questions,
  nav
})

export default (state, action) => {
  if (action.type === actions.logOut.toString()) {
    state = undefined
  }

  return appReducer(state, action)
}
