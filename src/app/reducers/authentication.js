import { handleActions } from "redux-actions"
import actions from "../actions/auth"

export const initialState = {
  loggedInUser: null,
  logInProgress: false,
  logInError: ""
}

export default handleActions(
  {
    [actions.logIn.start]: state => ({ ...state, logInProgress: true }),

    [actions.logIn.success]: (state, { payload }) => ({
      ...state,
      loggedInUser: payload.user,
      logInProgress: false,
      logInError: ""
    }),

    [actions.logIn.error]: (state, { payload }) => ({
      ...state,
      loggedInUser: null,
      logInProgress: false,
      logInError: payload.error
    }),

    [actions.logIn.clearError]: state => ({
      ...state,
      logInError: ""
    }),

    [actions.logOut]: (state, { payload }) => ({
      ...initialState
    })
  },
  initialState
)
