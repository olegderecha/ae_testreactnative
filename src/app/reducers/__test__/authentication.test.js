import reducer, { initialState } from "../authentication"
import actions from "../../actions/auth"

describe("authentication reducer", () => {
  it("has initial state", () => {
    const initialState = reducer(undefined, { type: undefined })
    expect(initialState).not.toBeNull()
  })

  it("handles actions/logIn/start action", () => {
    const expected = {
      ...initialState,
      logInProgress: true
    }

    const actual = reducer(initialState, {
      type: actions.logIn.start,
      payload: {}
    })

    expect(actual).toEqual(expected)
  })

  it("handles actions/logIn/success action", () => {
    const user = { id: 1, username: "John Doe" }
    const expected = {
      ...initialState,
      loggedInUser: user,
      logInProgress: false,
      logInError: ""
    }

    const actual = reducer(initialState, {
      type: actions.logIn.success,
      payload: { user }
    })

    expect(actual).toEqual(expected)
  })

  it("handles actions/logIn/error action", () => {
    const error = "Network connection"
    const expected = {
      ...initialState,
      loggedInUser: null,
      logInProgress: false,
      logInError: error
    }

    const actual = reducer(initialState, {
      type: actions.logIn.error,
      payload: { error }
    })

    expect(actual).toEqual(expected)
  })

  it("handles actions/logIn/clearError action", () => {
    const expected = {
      ...initialState,
      logInError: ""
    }

    const actual = reducer(initialState, {
      type: actions.logIn.clearError,
      payload: {}
    })

    expect(actual).toEqual(expected)
  })

  it("handles actions/logIn/logOut action", () => {
    const expected = { ...initialState }

    const actual = reducer(initialState, {
      type: actions.logIn.logOut,
      payload: {}
    })

    expect(actual).toEqual(expected)
  })
})
