import reducer, { initialState } from "../questions"
import actions from "../../actions/stackoverflow"

import questionMapper from "../../mappers/question"

describe("question reducer", () => {
  it("has initial state", () => {
    const initialState = reducer(undefined, { type: undefined })
    expect(initialState).not.toBeNull()
  })

  it("handles actions/stackoverflow/request", () => {
    const expected = {
      ...initialState,
      isFetching: true,
      error: "",
      items: []
    }

    const actual = reducer(initialState, {
      type: actions.stackoverflow.request,
      payload: {}
    })

    expect(actual).toEqual(expected)
  })

  it("handles actions/stackoverflow/success", () => {
    const items = [
      {
        title: "John",
        last_activity_date: 1513672961,
        answer_count: 10,
        is_answered: 0,
        score: 234,
        tags: ["reactjs", "react-native", "realm"]
      },
      {
        title: "Jane",
        last_activity_date: 1513671430,
        answer_count: 67,
        is_answered: 1,
        score: 678,
        tags: [
          "react-native",
          "redux",
          "integration",
          "deep-linking",
          "react-navigation"
        ]
      }
    ]

    const expected = {
      ...initialState,
      isFetching: false,
      error: "",
      items: [
        {
          title: "John",
          activityDate: "19 Dec, 2017 10:42:41",
          answers: 10,
          isAnswered: 0,
          score: 234,
          tags: "reactjs, react-native, realm"
        },
        {
          title: "Jane",
          activityDate: "19 Dec, 2017 10:17:10",
          answers: 67,
          isAnswered: 1,
          score: 678,
          tags:
            "react-native, redux, integration, deep-linking, react-navigation"
        }
      ]
    }

    const actual = reducer(initialState, {
      type: actions.stackoverflow.success,
      payload: { items: items.map(questionMapper) }
    })

    expect(actual).toEqual(expected)
  })

  it("handles actions/stackoverflow/error", () => {
    const expected = {
      ...initialState,
      isFetching: false,
      error: "Error text",
      items: []
    }

    const actual = reducer(initialState, {
      type: actions.stackoverflow.error,
      payload: { error: "Error text" }
    })

    expect(actual).toEqual(expected)
  })
})
