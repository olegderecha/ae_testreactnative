import { get } from "./api-client"

export default {
  getQuestions: () => {
    const site = "stackoverflow"
    const orderDirection = "desc"
    const sortColumn = "activity"
    const tag = "react-native"

    const request = `questions?site=${site}&order=${orderDirection}&sort=${sortColumn}&tagged=${tag}`
    return get(request)
  }
}
