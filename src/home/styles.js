import { StyleSheet } from "react-native"

import colors from "../app/colors"

export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: colors.white
  },

  homeContent: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },

  homeContentText: {
    fontSize: 30
  }
})
