import React from "react"
import { DrawerNavigator } from "react-navigation"

import HomeScreen from "./home"
import StackoverflowScreen from "../stackoverflow"
import LogoutScreen from "../logout"

import NavigationMenu from "./navigation-menu"

export default DrawerNavigator(
  {
    Home: { screen: HomeScreen },
    Stackoverflow: { screen: StackoverflowScreen },
    Logout: { screen: LogoutScreen }
  },
  {
    contentComponent: NavigationMenu
  }
)
