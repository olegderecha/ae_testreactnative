import React from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { translate } from "react-i18next"

import { NavigationActions } from "react-navigation"

import { View, Image, Text } from "react-native"
import { List, ListItem } from "react-native-elements"

import colors from "../../app/colors"
import images from "../../app/images"
import styles from "./styles"

import authActions from "../../app/actions/auth"

const getMenuItems = () => [
  {
    titleKey: "home",
    navigate: "Home",
    image: images.navigate.home
  },
  {
    titleKey: "stackoverflow",
    navigate: "Stackoverflow",
    image: images.navigate.stackoverflow
  },
  {
    titleKey: "logOut",
    navigate: "Logout",
    image: images.navigate.logOut
  }
]

const NavigationMenu = translate("home")(({ t, onItemPress }) =>
  <View style={styles.container}>
    <List containerStyle={styles.menuContainer}>
      {getMenuItems().map(item =>
        <ListItem
          containerStyle={styles.menuItem}
          key={item.titleKey}
          title={t("navigation." + item.titleKey)}
          titleStyle={styles.menuItemTitle}
          avatar={item.image}
          avatarStyle={styles.menuItemAvatar}
          avatarOverlayContainerStyle={{ backgroundColor: "transparent" }}
          hideChevron={true}
          underlayColor={colors.headerBackground}
          onPress={() => onItemPress(item.navigate)}
        />
      )}
    </List>
  </View>
)

NavigationMenu.PropTypes = {
  onItemPress: PropTypes.func.isRequired
}

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({
  onItemPress: route =>
    dispatch(NavigationActions.navigate({ routeName: route }))
})
export default connect(mapStateToProps, mapDispatchToProps)(NavigationMenu)
