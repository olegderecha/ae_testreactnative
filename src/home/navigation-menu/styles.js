import { StyleSheet, Platform } from "react-native"

import colors from "../../app/colors"

export default StyleSheet.create({
  menuContainer: {
    marginTop: 20,
    borderTopWidth: 0,
    borderBottomWidth: 0
  },

  container: {
    flex: 1,
    backgroundColor: colors.menuBackground,
    marginTop: 5
  },

  menuItem: {
    backgroundColor: colors.menuBackground
  },

  menuItemAvatar: {
    width: 32,
    height: 32
  },

  menuItemTitle: {
    color: colors.black,
    fontSize: 16
  }
})
