import React from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { translate } from "react-i18next"

import { View, Text } from "react-native"
import { Header, Content } from "./components"

import styles from "./styles"

const HomeScreen = translate("home")(({ username, t }) =>
  <View style={styles.screen}>
    <Header title={t("title")} />
    <Content username={username} />
  </View>
)

HomeScreen.PropTypes = {
  username: PropTypes.string.isRequired
}

const mapStateToProps = state => ({
  username: state.authentication.loggedInUser
    ? state.authentication.loggedInUser.username
    : null
})

const mapDispatchToProps = dispatch => ({})

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
