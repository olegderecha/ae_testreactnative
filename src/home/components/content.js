import React from "react"
import PropTypes from "prop-types"
import { translate } from "react-i18next"

import { View, Text } from "react-native"

import styles from "../styles"

export const Content = translate("home")(({ username, t }) =>
  <View style={styles.homeContent}>
    <Text style={styles.homeContentText}>
      {t("hello") + ", " + username + "!"}
    </Text>
  </View>
)

Content.PropTypes = {
  username: PropTypes.string.isRequired
}
