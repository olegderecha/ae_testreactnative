import { StyleSheet, Dimensions } from "react-native"

import colors from "../app/colors"

export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: colors.white
  },

  container: {
    flex: 1,
    flexDirection: "row",
    borderBottomColor: colors.lightGrey,
    borderBottomWidth: 2
  },

  panel: {
    padding: 10,
    width: 90,
    backgroundColor: colors.ultraLightGrey
  },

  panelContent: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },

  panelItem: {
    flex: 1,
    flexDirection: "row"
  },

  description: {
    flex: 1,
    padding: 10,
    backgroundColor: colors.white
  },

  title: {
    paddingBottom: 10,
    color: colors.questionTitle,
    fontWeight: "bold",
    fontSize: 16
  },

  tagsAndActivityContainer: {
    flex: 1,
    flexDirection: "row"
  },

  tags: {
    color: colors.questionTag,
    fontSize: 16,
    marginBottom: 10
  },

  lastActivity: {
    color: colors.questionTag,
    fontSize: 16
  },

  votes: {
    color: colors.questionVote,
    fontSize: 22
  },

  votesImage: {
    width: 25,
    height: 25,
    marginLeft: 8,
    marginTop: 2
  },

  answersImage: {
    width: 25,
    height: 25,
    marginLeft: 8,
    marginTop: 4
  },

  answerAccepted: {
    backgroundColor: colors.lightGreen
  },

  answeredColor: {
    color: colors.darkGreen
  },

  errorContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  }
})
