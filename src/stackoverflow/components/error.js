import React from "react"
import PropTypes from "prop-types"

import { View, Text } from "react-native"

import styles from "../styles"

export const Error = ({ text }) =>
  <View style={styles.screen}>
    <View style={styles.errorContainer}>
      <Text>
        {text}
      </Text>
    </View>
  </View>

Error.propTypes = {
  text: PropTypes.string.isRequired
}
