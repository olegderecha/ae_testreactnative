import React from "react"
import PropTypes from "prop-types"

import { View, Text, Image } from "react-native"

import styles from "../styles"
import images from "../../app/images"

export class Question extends React.Component {
  static propTypes = {
    item: PropTypes.object.isRequired
  }

  render() {
    const { item } = this.props

    const hasAnswers = item.answers > 0
    const answerNotAcceptedImage = hasAnswers
      ? images.question.answersYes
      : images.question.answersNo
    const answerImage = item.isAnswered
      ? images.question.answerAccepted
      : answerNotAcceptedImage
    const acceptedStyle = item.isAnswered ? styles.answerAccepted : ""
    const answeredStyle = hasAnswers ? styles.answeredColor : ""

    return (
      <View style={styles.container}>
        <View style={[styles.panel, acceptedStyle]}>
          <View style={styles.panelContent}>
            <View style={styles.panelItem}>
              <Text style={styles.votes}>
                {item.score}
              </Text>
              <Image
                style={styles.votesImage}
                source={images.question.votes}
                resizeMode="contain"
              />
            </View>
            <View style={styles.panelItem}>
              <Text style={[styles.votes, answeredStyle]}>
                {item.answers}
              </Text>
              <Image
                style={styles.answersImage}
                source={answerImage}
                resizeMode="contain"
              />
            </View>
          </View>
        </View>
        <View style={styles.description}>
          <Text style={styles.title}>
            {item.title}
          </Text>
          <Text style={styles.tags}>
            {item.tags}
          </Text>
          <Text style={styles.lastActivity}>
            {item.activityDate}
          </Text>
        </View>
      </View>
    )
  }
}
