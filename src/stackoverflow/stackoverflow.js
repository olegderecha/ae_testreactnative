import React from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { translate } from "react-i18next"

import { View, Text, Image, FlatList } from "react-native"
import { Header, Question, Error, ActivitySpinner } from "./components"

import styles from "./styles"
import images from "../app/images"

@translate("stackoverflow")
export default class StackoverflowScreen extends React.Component {
  static propTypes = {
    isFetching: PropTypes.bool.isRequired,
    items: PropTypes.array.isRequired,
    error: PropTypes.string.isRequired,
    onGetQuestions: PropTypes.func.isRequired
  }

  componentWillMount() {
    this.props.onGetQuestions()
  }

  render() {
    return (
      <View style={styles.screen}>
        <Header title={this.props.t("title")} />
        {!this.props.error.length &&
          <View style={styles.screen}>
            <FlatList
              keyboardDismissMode="on-drag"
              keyboardShouldPersistTaps="always"
              keyExtractor={item => item.title}
              data={this.props.items}
              renderItem={({ item }) => <Question item={item} />}
            />
            <ActivitySpinner isVisible={this.props.isFetching} />
          </View>}
        {!!this.props.error.length && <Error text={this.props.error} />}
      </View>
    )
  }
}
