import { connect } from "react-redux"
import { NavigationActions } from "react-navigation"

import StackoverflowScreen from "./stackoverflow"

import { getQuestions } from "../app/actions/stackoverflow"

const mapStateToProps = (state, props) => ({
  isFetching: state.questions.isFetching,
  items: state.questions.items,
  error: state.questions.error
})
const mapDispatchToProps = (dispatch, props) => ({
  onGetQuestions: () => dispatch(getQuestions())
})

export default connect(mapStateToProps, mapDispatchToProps)(StackoverflowScreen)
