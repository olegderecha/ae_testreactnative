import { connect } from "react-redux"
import { NavigationActions } from "react-navigation"
import { LoginScreen } from "./login"

import actions, { logIn, logInSso } from "../app/actions/auth"

const mapStateToProps = state => ({
  authentication: state.authentication
})

const mapDispatchToProps = dispatch => ({
  onLogIn: (username, password) => {
    dispatch(logIn({ username, password }))
  },
  onClearError: () => dispatch(actions.logIn.clearError())
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
