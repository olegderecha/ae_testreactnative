import React from "react"
import PropTypes from "prop-types"

import { View, Image, TextInput } from "react-native"

import styles from "../styles"

export const LoginFormElement = ({
  value,
  image,
  secureTextEntry,
  placeholder,
  onChange,
  ...props
}) =>
  <View style={styles.loginFormElement}>
    <Image style={styles.loginFormIcon} source={image} resizeMode="contain" />

    <TextInput
      defaultValue={value}
      secureTextEntry={secureTextEntry}
      style={styles.loginInput}
      underlineColorAndroid="transparent"
      placeholder={placeholder}
      onChangeText={onChange}
      {...props}
    />
  </View>

LoginFormElement.propTypes = {
  value: PropTypes.string.isRequired,
  image: PropTypes.number.isRequired,
  secureTextEntry: PropTypes.bool,
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
}
