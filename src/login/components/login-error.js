import React from "react"
import PropTypes from "prop-types"

import { View, Text } from "react-native"

import styles from "../styles"

export const LoginError = ({ text }) =>
  !!text.length &&
  <View style={styles.errorContainer}>
    <Text style={styles.error}>
      {text}
    </Text>
  </View>

LoginError.propTypes = {
  text: PropTypes.string.isRequired
}
