import React from "react"
import PropTypes from "prop-types"
import { translate } from "react-i18next"

import { Image, Text, View } from "react-native"
import { LoginFormElement } from "./login-form-element"

import styles from "../styles"
import images from "../../app/images"

export const LoginForm = translate(
  "login"
)(({ username, password, t, onUsernameChanged, onPasswordChanged }) =>
  <View>
    <View style={styles.loginForm}>
      <LoginFormElement
        value={username}
        image={images.login.username}
        placeholder={t("username")}
        onChange={onUsernameChanged}
      />

      <LoginFormElement
        value={password}
        image={images.login.password}
        placeholder={t("password")}
        secureTextEntry={true}
        onChange={onPasswordChanged}
      />
    </View>
  </View>
)

LoginForm.propTypes = {
  username: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  onUsernameChanged: PropTypes.func.isRequired,
  onPasswordChanged: PropTypes.func.isRequired
}
