import React from "react"
import PropTypes from "prop-types"
import { translate } from "react-i18next"

import { Text } from "react-native"

import {
  LoginForm,
  LoginError,
  KeyboardAvoiding,
  ActivitySpinner
} from "./components"

import { Button } from "react-native-elements"
import { LinearGradient } from "expo"

import colors from "../app/colors"
import styles from "./styles"

@translate("login")
export class LoginScreen extends React.Component {
  static propTypes = {
    authentication: PropTypes.object.isRequired,
    onLogIn: PropTypes.func.isRequired,
    onClearError: PropTypes.func.isRequired
  }

  state = {
    username: __DEV__ ? "oleg" : "",
    password: __DEV__ ? "oleg" : ""
  }

  logIn = () => this.props.onLogIn(this.state.username, this.state.password)

  clearError = () => {
    if (this.props.authentication.logInError.length) {
      this.props.onClearError()
    }
  }

  onUsernameChanged = username => {
    this.setState({ username })
    this.clearError()
  }

  onPasswordChanged = password => {
    this.setState({ password })
    this.clearError()
  }

  render() {
    const { t } = this.props

    return (
      <LinearGradient style={styles.logInContainer} colors={colors.linear}>
        <KeyboardAvoiding>
          <Text style={styles.welcome}>
            {t("welcome")}
          </Text>

          <LoginError text={this.props.authentication.logInError} />

          <LoginForm
            username={this.state.username}
            password={this.state.password}
            onUsernameChanged={this.onUsernameChanged}
            onPasswordChanged={this.onPasswordChanged}
          />

          <Button
            buttonStyle={styles.loginButton}
            fontWeight="bold"
            backgroundColor={colors.blueSky}
            title={t("login")}
            onPress={this.logIn}
          />
        </KeyboardAvoiding>

        <ActivitySpinner isVisible={this.props.authentication.logInProgress} />
      </LinearGradient>
    )
  }
}
