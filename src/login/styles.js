import { StyleSheet } from "react-native"

import colors from "../app/colors"

export default StyleSheet.create({
  logInContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-around",
    padding: 32
  },

  welcome: {
    alignSelf: "center",
    color: colors.blueDark,
    fontWeight: "bold",
    marginBottom: 40,
    fontSize: 22,
    backgroundColor: "transparent"
  },

  loginForm: {
    marginTop: 16
  },

  loginFormElement: {
    padding: 8,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    height: 56
  },

  loginInput: {
    marginBottom: 5,
    borderWidth: 1,
    borderColor: colors.buttonBorder,
    padding: 8,
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    height: 45
  },

  loginFormIcon: {
    width: 32,
    height: 32,
    marginTop: -4,
    marginRight: 16
  },

  loginButton: {
    height: 48,
    width: 150,
    marginTop: 30,
    backgroundColor: colors.blueSky,
    borderRadius: 8,
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 8,
    alignSelf: "center"
  },

  errorContainer: {
    padding: 10,
    backgroundColor: colors.errorBackground
  },

  error: {
    color: colors.white
  }
})
